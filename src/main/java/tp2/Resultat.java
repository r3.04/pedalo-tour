package tp2;

import java.util.*;

public class Resultat {

	public static Participant[] participants = {
			new Participant("Dupont", "Paul"),
			new Participant("Durand", "Pierre"),
			new Participant("Smith", "Aline"),
			new Participant("Riviere", "Louis"),
			new Participant("Audic", "Laura") };


	Map<Participant, Temps[]> resultats;
	Map<String, Set<Participant>> annuaire;

	public Resultat() {
		resultats = new TreeMap<>();
		annuaire = new TreeMap<>();
		for (int i=0;i<participants.length;i++) {
			if (annuaire.containsKey(participants[i].nom)) {
				annuaire.get(participants[i].nom).add(participants[i]);
			} else {
				Set<Participant> set = new HashSet<>();
				set.add(participants[i]);
				annuaire.put(participants[i].nom, set);
			}
			resultats.put(participants[i], new Temps[6]);
		}
	}

	public boolean ajout(Participant participant, Temps temps, int etape) {
		if (! resultats.containsKey(participant) || (etape < 0) || (etape >= 6)) return false;
		this.resultats.get(participant)[etape]=temps;
		return true;
	}
	
	
	public String resultat(Participant participant, int etape) {
		Temps[] temps = this.resultats.get(participant);
		if (temps == null) return null;
		else {
			StringBuilder resultat = new StringBuilder();
			resultat.append(participant).append(' ').append(etape).append("->");
			if (temps[etape] == null) resultat.append("pas présent");
			else resultat.append(temps[etape]);
			return resultat.toString();
		}
	}

	public String afficheEtape(int etape) {
		StringBuilder cumul = new StringBuilder("Etape ").append(etape).append('\n');
		for (Map.Entry<Participant, Temps[]> entry: resultats.entrySet()) {
			cumul.append(entry.getKey()).append(" : ").append(entry.getValue()[etape]).append('\n');
		}
		return cumul.toString();
	}

	@Override
	public String toString() {
		StringBuilder cumul = new StringBuilder();
		for (Map.Entry<Participant, Temps[]> entry: resultats.entrySet()) {
			Temps[] scores = entry.getValue();
			cumul.append(entry.getKey()).append(" : ").append(Arrays.toString(scores)).append('\n');
		}
		return cumul.toString();
	}

	public Temps tempsGlobal(Participant inscrit) {
		Temps global = new Temps(0);
		Temps[] scores = resultats.get(inscrit);
		for (int etape = 0; etape < 6; etape++) {
			global = Temps.add(global, scores[etape]);
		}
		return global;
	}

	public Set<Participant> rechercheSequentielle(String nom) {
		Set<Participant> returned = new HashSet<>();
		for (Participant participant : resultats.keySet()) {
			if (nom == participant.nom) {
				returned.add(participant);
			}
		}
		return returned;
	}

	public Set<Participant> recherche(String nom) {
		if (annuaire.containsKey(nom)) return annuaire.get(nom);
		else return new HashSet<>();
	}

	public static void main(String[] args) {
		Resultat resultats = new Resultat();
		resultats.ajout(participants[0], new Temps(7843), 0);
		System.out.println(resultats);
	}
}
